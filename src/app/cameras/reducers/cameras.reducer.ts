import { CamerasActionTypes, CamerasActionsUnion } from '../actions/cameras.actions';
import {Camera} from '../../model/camera';

export interface IState  {
  list: Camera[];
}

const initialState: IState = {
  list: []
};

export function reducer(state = initialState, action: CamerasActionsUnion): IState {
  switch (action.type) {

    case CamerasActionTypes.SET_LIST: {
      return {
        list: [...action.payload]
      };
    }

    default: return state;
  }
}
