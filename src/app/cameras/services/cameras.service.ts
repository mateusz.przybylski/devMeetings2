import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {config} from '../../config/app.config';
import {Camera} from '../../model/camera';


@Injectable({
  providedIn: 'root'
})
export class CamerasService {

  constructor(private http: HttpClient) {
    this.http = http;
  }

  getCameras(): Observable<Camera[]> {
    return this.http.get<Camera[]>(config.apiUrl + 'camera');
  }
}
