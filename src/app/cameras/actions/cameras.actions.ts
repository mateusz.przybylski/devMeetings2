import { Action } from '@ngrx/store';
import {Camera} from '../../model/camera';

export enum CamerasActionTypes {
  INIT_LIST = 'Cameras list initialization',
  SET_LIST = 'Set cameras list'
}

export class Init implements Action {
  readonly type = CamerasActionTypes.INIT_LIST;
}

export class SetList implements Action {

  constructor(public payload: Camera[]) {
  }
  readonly type = CamerasActionTypes.SET_LIST;
}

export type CamerasActionsUnion = Init | SetList;
