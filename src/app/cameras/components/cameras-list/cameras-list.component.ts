import { Component, OnInit } from '@angular/core';
import {CamerasService} from '../../services/cameras.service';
import {Camera} from '../../../model/camera';
import {Observable} from 'rxjs';
import {select, Store} from '@ngrx/store';
import * as CameraActions from '../../actions/cameras.actions';
import * as fromRoot from '../../reducers';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-cameras-list',
  templateUrl: './cameras-list.component.html',
  styleUrls: ['./cameras-list.component.css']
})
export class CamerasListComponent implements OnInit {

  cameras: Observable<Camera[]>;

  constructor(private camerasService: CamerasService, private store: Store<any>) {
    this.camerasService = camerasService;

    this.cameras = this.store
      .pipe(
        select(fromRoot.getEventEntitiesState),
        map(cameras => cameras.list)
      );
  }

  ngOnInit(): void {
    this.camerasService.getCameras()
      .subscribe(
        cameras => {
        this.store.dispatch(new CameraActions.SetList(cameras));
      });
  }
}
